<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>NewBiz Bootstrap Template - Index</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="/img/logo.png" rel="icon">
  <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@500&display=swap" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NewBiz - v2.0.0
  * Template URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header  ======= -->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-right">
        <!-- Uncomment below if you prefer to use an text logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="/img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-left d-none d-lg-block">
        <ul>
          <li><a href="#intro">الصفحه الرئيسية</a></li>
          <li> <a href="#about">عنا</a></li>
          <li><a href="#services">خدماتنا</a></li>
          <li><a href="#portfolio">أعمالنا</a></li>
          <li><a href="#team">فريق العمل</a></li>
          <li class="active"><a href="#contact">تواصل معنا</a></li>

        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  <!-- ======= Intro Section ======= -->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <img src="/img/intro-img.svg" alt="" class="img-fluid">
      </div>

      <div class="intro-info">
        <h2>جميعة البر بالاحساء<br><h1>مركز الشعبة بالمبرز</h1><br></h2>
        <div>
          <a href="#services" class="btn-get-started scrollto">خدماتنا</a>
          <a href="#about" class="btn-get-started scrollto">عنا</a>
         
        </div>
      </div>

    </div>
  </section><!-- End Intro Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>عن الجمعيه</h3>
          <p> الجمعية من أوائل الجمعيات الخيرية في العالم الإسلامي التي أحيت سنة المصطفى صلى الله عليه وسلم في إنشاء المشاريع الوقفية الصغيرة. مع التركيز في عملها على المشاريع التنموية التي تحقق استدامة وتهدف الجمعية للقيام بأعمال التنمية للمجتمعات الأقل حظاً مستهدفة بذلك الفئات الاجتماعية الأكثر احتياجاً والمرضى والأيتام ومنكوبي الكوارث والمجاعات والقيام بكافة أنشطة البر والخير.

          </p>
        </header>

        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <p>
              منذ عام 1981, بأياديكم الكريمة بذرتم البذرة لبداية عمل خيري وإنساني في إفريقيا .. فبفضل الله تعالى ثم بدعمكم أيها الخيرين وفقنا الله لانقاذ مئات الآلاف من البشر من غول الموت جوعاً في مجاعة عام 1984 / 1985 الشهيرة وغيرها من المجاعات … واستطعتم إنتشال أيتام وفقراء من الشوارع يجوع الواحد منهم يوماً أو يومين .. فوفرتم لهم سبل الحياة الكريمة .. ويسرتم لهم التعليم ، ليتخرجوا بعد سنوات أطباء ومهندسون ومعلمون ومحامون وتولى البعض منهم مناصب عليا.            </p>

            <div class="icon-box wow fadeInUp">
              <div class="icon"><i class="fa fa-shopping-bag"></i></div>
              <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
              <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-photo"></i></div>
              <h4 class="title"><a href="">Magni Dolores</a></h4>
              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>

            <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
              <div class="icon"><i class="fa fa-bar-chart"></i></div>
              <h4 class="title"><a href="">Dolor Sitema</a></h4>
              <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div>

          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
            <img src="/img/about-img.jpg" class="img-fluid" alt="">
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp">
            <img src="/img/about-extra-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>ماذا نقدم لكم </h4>
            <p>تقدم جمعية الشعبة الخيرية للخدمات الإجتماعية المساعدات لرفع المستوى الصحي والثقافي والتعليمي والإجتماعي والأقتصادي، </p>
            <p>
              وذلك عن طريق إنشاء المشروعات التي تنهض بالطفولة والأمومة، وتقديم المساعدة لكل من يستحق المساعدة حسب النطاق المعطى لها شرعاً ونظاماً وقانوناً .
            </p>
          </div>
        </div>


      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>برامج وأنشطة الجمعية</h3>
          <p>محمد </p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="">الزكوات</a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-bookmarks-outline" style="color: #e9bf06;"></i></div>
              <h4 class="title"><a href="">	صدقة ( جارية )</a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-paper-outline" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="">تفريج كربة</a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-speedometer-outline" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="">السلة الغذائية</a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>

          <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-world-outline" style="color: #d6ff22;"></i></div>
              <h4 class="title"><a href="">ترميم المنازل</a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon"><i class="ion-ios-clock-outline" style="color: #4680ff;"></i></div>
              <h4 class="title"><a href="">مساعدات مقطوعة عينية أو مالية </a></h4>
              <p class="description"><a href="">التفاصيل</a></p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="wow fadeIn">
      <div class="container">
        <header class="section-header">
          <h3>المبادرات  </h3>
          <p>  يمكنك التبرع للمشاريع كفالة حاج و كسوة الفقراء و مساعدات ببنائ المشاريع الخيرية عن طريق التواصل مع فريق العمل
          </p>
        </header>

        <div class="row row-eq-height justify-content-center">

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
              <i class="fa fa-diamond"></i>
              <div class="card-body">
                <h5 class="card-title">كسوة للفقراء</h5>
                <p class="card-text">يمكنك التبرع بقيمة 150 للسهم الواحد</p>
                <a href="#" class="readmore">التفاصيل  </a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
              <i class="fa fa-language"></i>
              <div class="card-body">
                <h5 class="card-title">كفالة الحاج</h5>
                <p class="card-text">يمكنك التبرع بقيمة 150 للسهم الواحد</p>
                <a href="#" class="readmore">التفاصيل </a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
              <i class="fa fa-object-group"></i>
              <div class="card-body">
                <h5 class="card-title">مساعدات</h5>
                <p class="card-text">يمكنك التبرع بقيمة 150 للسهم الواحد</p>
                <a href="#" class="readmore">التفاصيل </a>
              </div>
            </div>
          </div>

        </div>

        <header class="section-header">
          <h3>إحصائيات</h3>
        </header>
        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">30,400</span>
            <p>كسوة للفقراء </p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">13,501</span>
            <p>كفالة الحاج</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">3,064</span>
            <p>مشاريع تم انشائها</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>مشاريع تحت الانشاء</p>
          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="clearfix">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title"> أعمالنا وفعاليات</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              
              <li data-filter=".filter-app">مساعدات رمضانيه</li>
              <li data-filter=".filter-card">مساعدات للمدارس</li>
              <li data-filter=".filter-web">بالترميم</li>
              <li data-filter="*" class="filter-active">الكل</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/app1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد</a></h4>
                <p>مساعدات رمضانيه</p>
                <div>
                  <a href="/img/portfolio/app1.jpg" data-gall="portfolioGallery" title="App 1" class="venobox link-preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/web3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد </a></h4>
                <p>ترميم</p>
                <div>
                  <a href="/img/portfolio/web3.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Web 3"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/app2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد</a></h4>
                <p>مساعدات رمضانيه</p>
                <div>
                  <a href="/img/portfolio/app2.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="App 2"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/card2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد </a></h4>
                <p>للمدارس</p>
                <div>
                  <a href="/img/portfolio/card2.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Card 2"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/web2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأ المزيد</a></h4>
                <p>ترميم</p>
                <div>
                  <a href="/img/portfolio/web2.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Web 2"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/app3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#"> اقرأالمزيد</a></h4>
                <p>مساعدات رمضانيه</p>
                <div>
                  <a href="/img/portfolio/app3.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="App 3"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/card1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد </a></h4>
                <p>للمدارس</p>
                <div>
                  <a href="/img/portfolio/card1.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Card 1"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/card3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأالمزيد </a></h4>
                <p>للمدارس</p>
                <div>
                  <a href="public/img/portfolio/card3.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Card 3"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="/img/portfolio/web1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="#">اقرأ المزيد</a></h4>
                <p>ترميم</p>
                <div>
                  <a href="/img/portfolio/web1.jpg" class="venobox link-preview" data-gall="portfolioGallery" title="Web 1"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>ماذا قالوا عن الاحسان</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">

              <div class="testimonial-item">
                <img src="/img/testimonial-1.jpg" class="testimonial-img" alt="">
                <h3>الشخص الاول</h3>
                <h4>من صور مساعدة الناس</h4>
                <p>
                  إنّ نفعَ الناس لا يقتصر على المساعدة المادية، وإنّما يتجاوزها إلى تحرّك المسلم، وسعيه مع أخيه لقضاء حاجته، أي لإتمام هذه الحاجة، كأن يعينه على إتمام معاملة قانونية، أو مصاحبته في زيارة، أو سفر، أو إعانته في إصلاح شيء، أو إعداده،[٦] ومن صور ذلك أيضاً إطعام الجائع، بتقديم ما يشبعه، ويذهب عنه الجوع من الطعام، وعيادة المريض، وتخليص الأسير والمحبوس ظلماً، والتبرع بالمال للمحتاج، وإجابة السائل، والصبر على قضاء حاجته، والمساهمة في قضاء دين المدين، والإصلاح بين المتخاصمين، وتأليف القلوب، وغيرها من أعمال البرّ.                 </p>
              </div>

              <div class="testimonial-item">
                <img src="/img/testimonial-2.jpg" class="testimonial-img" alt="">
                <h3>الشخص الثاني </h3>
                <h4>ادب قضاء حوائج الناس </h4>
                <p>
                  أولى هذا الآداب إخلاصُ السّاعي، وقصد الله تعالى في العمل، وترك المنّ بها، والأفضل ستر العمل، وإتمامُه ، فإتمامه من إتقانه، ومن هذه الآداب أيضاً طلبُ المحتاج المعونة من الكريم، وترك طلبها من اللئيم، فمن طلب من شخص أمراً فقد أحسن الظّن به، وكذلك الثّناءُ، والشّكر، وهو من الآداب الخاصة بصاحب الحاجة المفتقر إلى الناس، فيؤدي حقَّ شكر من قضى له حاجته، إذا لم يستطع أن يكافئه.                </p>
              </div>

              <div class="testimonial-item">
                <img src="/img/testimonial-3.jpg" class="testimonial-img" alt="">
                <h3>الشخص الثالث</h3>
                <h4>مساعدة الآخرين في السنّة النبوية</h4>
                <p>
                  ممّا ورد في نفع الناس، وكشف كربهم حديث النبي صلى الله عليه وسلم: (من نفَّسَ عن مؤمنٍ كُربةً من كُرَبِ الدنيا، نفَّسَ اللهُ عنه كُربةً من كُرَبِ يومِ القيامةِ. ومن يسّرَ على معسرٍ، يسّرَ اللهُ عليه في الدنيا والآخرةِ. ومن سترَ مسلمًا، ستره اللهُ في الدنيا والآخرةِ. واللهُ في عونِ العبدِ ما كان العبدُ في عونِ أخيه)،[١] ومنها أيضاً قوله صلى الله عليه وسلم: (المسلمُ أخو المسلمِ، لا يظلِمُه ولا يُسلِمُه. من كان في حاجةِ أخيه ، كان اللهُ في حاجتِه. ومن فرَّج عن مسلمٍ كُرْبةً، فرَّج اللهُ عنه بها كُرْبةً من كُرَبِ يومِ القيامةِ. ومن ستر مسلمًا، ستره اللهُ يومَ القيامةِ).[٢] 
                </p>
              </div>

              <div class="testimonial-item">
                <img src="/img/testimonial-4.jpg" class="testimonial-img" alt="">
                <h3>الشخص الرابع</h3>
                <h4>خير النَّاس أنفعهم للنَّاس</h4>
                <p>
                  (أحبُّ الناسِ إلى اللهِ أنْفَعُهُمْ لِلنَّاسِ، وأحبُّ الأعمالِ إلى اللهِ عزَّ وجلَّ سُرُورٌ يدْخِلُهُ على مسلمٍ، أوْ يكْشِفُ عنهُ كُرْبَةً، أوْ يقْضِي عنهُ دَيْنًا، أوْ تَطْرُدُ عنهُ جُوعًا، ولأنْ أَمْشِي مع أَخٍ لي في حاجَةٍ أحبُّ إِلَيَّ من أنْ اعْتَكِفَ في هذا المسجدِ، يعني مسجدَ المدينةِ شهرًا، ومَنْ كَفَّ غضبَهُ سترَ اللهُ عَوْرَتَهُ، ومَنْ كَظَمَ غَيْظَهُ، ولَوْ شاءَ أنْ يُمْضِيَهُ أَمْضَاهُ مَلأَ اللهُ قلبَهُ رَجَاءً يومَ القيامةِ، ومَنْ مَشَى مع أَخِيهِ في حاجَةٍ حتى تتَهَيَّأَ لهُ أَثْبَتَ اللهُ قَدَمَهُ يومَ تَزُولُ الأَقْدَامِ، وإِنَّ سُوءَ الخُلُقِ يُفْسِدُ العَمَلَ، كما يُفْسِدُ الخَلُّ العَسَلَ).   </p>
              </div>

              <div class="testimonial-item">
                <img src="/img/testimonial-5.jpg" class="testimonial-img" alt="">
                <h3> الشخص الخامس</h3>
                <h4>ثمرت نفع الناس</h4>
                <p>
                  جاءت كثيرٌ من النُّصوص الشرعيَّة حاثَّةً على التعاضد والتَّآخي وإسداء المعروف، ومدِّ يد العون لمن يحتاج، ومن ذلك قول الله تعالى: (يَا أَيُّهَا الَّذِينَ آمَنُوا لَا تُحِلُّوا شَعَائِرَ اللَّهِ وَلَا الشَّهْرَ الْحَرَامَ وَلَا الْهَدْيَ وَلَا الْقَلَائِدَ وَلَا آمِّينَ الْبَيْتَ الْحَرَامَ يَبْتَغُونَ فَضْلًا مِّن رَّبِّهِمْ وَرِضْوَانًا وَإِذَا حَلَلْتُمْ فَاصْطَادُوا وَلَا يَجْرِمَنَّكُمْ شَنَآنُ قَوْمٍ أَن صَدُّوكُمْ عَنِ الْمَسْجِدِ الْحَرَامِ أَن تَعْتَدُوا وَتَعَاوَنُوا عَلَى الْبِرِّ وَالتَّقْوَىٰ وَلَا تَعَاوَنُوا عَلَى الْإِثْمِ وَالْعُدْوَانِ وَاتَّقُوا اللَّهَ إِنَّ اللَّهَ شَدِيدُ الْعِقَابِ).                 </p>
              </div>

            </div>

          </div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <section id="team">
      <div class="container">
        <div class="section-header">
          <h3>فريق العمل </h3>
          <p>يمكنك التواصل مع فريق العمل في حال وجود اي مشكله او اقتراح</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="/img/user-silhouette.svg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Walter White</h4>
                  <span>Chief Executive Officer</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="/img/hair-cut.svg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Sarah Jhonson</h4>
                  <span>Product Manager</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="/img/user (1).svg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>William Anderson</h4>
                  <span>CTO</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div class="member">
              <img src="/img/user.svg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Amanda Jepson</h4>
                  <span>Accountant</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="section-bg">

      <div class="container">

        <div class="section-header">
          <h3>الداعمون</h3>
          <p>بفضل من الله ثم مجهودهم و دعمهم المستمر هؤلاء هم الداعمون الرسميون </p>
        </div>

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-1.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-2.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-3.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-4.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-5.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-6.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-7.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="/img/clients/client-8.png" class="img-fluid" alt="">
            </div>
          </div>

        </div>

      </div>

    </section><!-- End Clients Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact">
      <div class="container-fluid">

        <div class="section-header">
          <h3>تواصل معنا</h3>
        </div>

        <div class="row wow fadeInUp">

          <div class="col-lg-6">
            <div class="map mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.853154541524!2d49.59140141485006!3d25.409725583797762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e379666052d6e2b%3A0x7850b8da0d1e70f1!2z2KzZhdi52YrYqSDYp9mE2KjYsSDYp9mE2K7Zitix2YrYqSAtINmF2LHZg9iyINin2YTYtNi52KjYqQ!5e0!3m2!1sen!2ssa!4v1591300315946!5m2!1sen!2ssa" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>            </div>
          </div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-md-5 info">
                <i class="ion-ios-location-outline"></i>
                <p>وسط الأحساء، الهفوف ، الشعبة‎ 36341</p>
              </div>
              <div class="col-md-4 info">
                <i class="ion-ios-email-outline"></i>
                <p><a href="mailto: shabah@ahsaber.org.sa" >shabah@ahsaber.org.sa</a></p>
              </div>
              <div class="col-md-3 info">
                <i class="ion-ios-telephone-outline"></i>
                <p> <a href="tel:+966135877110">+966135877110</a></p>
              </div>
            </div>

            <div class="form">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                    <div class="validate"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="mb-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">إرسال</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

 <!-- ======= Footer ======= -->
 <footer id="footer">
  <div class="footer-top">
    <div class="container">


      <div class="row">

        <div class="col-lg-4 col-md-6 footer-info">
          <h3>عن الجمعيه</h3>
          <p>
            لجمعية من أوائل الجمعيات الخيرية في العالم الإسلامي التي أحيت سنة المصطفى صلى الله عليه وسلم في إنشاء المشاريع الوقفية الصغيرة. مع التركيز في عملها على المشاريع التنموية التي تحقق استدامة وتهدف الجمعية للقيام بأعمال التنمية للمجتمعات الأقل حظاً مستهدفة بذلك الفئات الاجتماعية الأكثر احتياجاً والمرضى والأيتام ومنكوبي الكوارث والمجاعات والقيام بكافة أنشطة البر والخير 
          </p>
        </div>

        <div class="col-lg-2 col-md-6 footer-links">
          <h4>روابط مفيدة</h4>
          <ul>
            <li><a href="#">الصفحه الرئيسية</a></li>
            <li><a href="#"> عنا</a></li>
            <li><a href="#">خدماتنا</a></li>
            <li><a href="#">فريق العمل</a></li>
            <li><a href="#">الداعمون</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-contact">
          <h4>تواصل معنا</h4>
          <p>
            36341 وسط الاحساء <br>
            الاحساء,الشعبة<br>
            المملكة العربية السعودية <br>
            <strong>Phone:</strong> <a href="tel:+966135877110">+966135877110</a><br>
            <strong>Email:</strong> <a href="mailto: shabah@ahsaber.org.sa" >shabah@ahsaber.org.sa</a><br>
          </p>

          <div class="social-links">
            <a href="https://twitter.com/ber_msc1?lang=ar" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://ar-ar.facebook.com/ber.msc1/" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
          </div>

        </div>

        <div class="col-lg-3 col-md-6 footer-newsletter">
          <h4>اشترك معنا</h4>
          <p>
            للحصول على الاخبار اول باول يرجى وضع البريد الاكتروني 
          </p>
          <form action="" method="post">
            <input type="email" name="email"><input type="submit" value="اشتراك">
          </form>
        </div>

      <div class="copyright"

        <div class="footer-top">
          <div class="container">
         
         </div>
          
        </div>

      </div>
      <div class="credits">
      
      </div>
    </div>
  </div>

  <div class="container">
    <div class="copyright">
      &copy; <strong><a href="https://cm.codes/">CUBE MASTER | إتقان مكعب</a></strong>
    </div>
    <div class="credits">
      
    </div>
  </div>
</footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="/vendor/php-email-form/validate.js"></script>
  <script src="/vendor/counterup/counterup.min.js"></script>
  <script src="/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="/vendor/wow/wow.min.js"></script>
  <script src="/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="/vendor/venobox/venobox.min.js"></script>

  <!-- Template Main JS File -->
  <script src="/js/main.js"></script>

</body>

</html>